import React, { PureComponent } from 'react';
import { getRandomColor } from '../utils';

import { connect } from 'react-redux';
import { RootState } from '../reducers/index';

type OwnProps = {
	$hover?: boolean;
	number?: string;
	direction?: string;
};

interface StateProps {}

type Props = OwnProps & StateProps;

class CarLocation extends PureComponent<Props> {
	color = getRandomColor();

	render() {
		return (
			<div
				style={{
					margin: '0 auto',
					width: '10px',
					height: '10px',
					borderRadius: '50%',
					background: this.color,
					border: '3px solid white'
				}}
			/>
		);
	}
}

const mapStateToProps = (state: RootState) => {
	return {};
};

export default connect(
	mapStateToProps,
	null
)(CarLocation);
