import React from 'react';
import { Coords } from 'google-map-react';

const CurrentLocationIcon: React.SFC<Coords> = () => (
	<div
		style={{
			width: '15px',
			height: '15px',
			borderRadius: '50%',
			border: '5px solid #C8D6EC',
			lineHeight: '15px'
		}}
	>
		<div
			style={{
				margin: '0 auto',
				width: '10px',
				height: '10px',
				borderRadius: '50%',
				background: '#4285F4',
				border: '3px solid white'
			}}
		/>
	</div>
);

export default CurrentLocationIcon;
